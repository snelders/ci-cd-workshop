# User acceptance testing

User acceptance testing is automatically validating the software from the user perspective.

## Goals

Validate the application from the user perspective. This is testing how to application is used by actual users.

## Approach

The user acceptance tests (end-to-end/e2e) are done with the JavaScript implementation of the 
[cucumber framework](https://github.com/cucumber/cucumber-js). This framework allows for tests to be written in human 
and machine readable **given-when-then** statements.

The directory [features](../features) contains the user acceptance tests for this application.  

## Exercise

In this exercise a user-acceptance test stage is created with a end-to-end test job that executes the cucumber tests. 
In the [package.json](../package.json) is a **e2** step defined that calls the cucumber tests. These tests will validate
the deployed endpoint.

### Step 1: End-to-end testing

Create a e2e job and a uat stage in the continuous delivery pipeline. Add the following parts to 
the [.gitlab-ci.yml](../.gitlab-ci.yml) 

```yaml
stages:
# Disable
#  - sample
  - qa
  - build
  - test
  - uat

# The user acceptance end-to-end test job
e2e:
  stage: uat
  script:
  - npm run e2e
```

The stage **uat** is an new logical divider of steps within the continuous delivery pipeline. The job **e2e** is 
actual command that starts the end-to-end testing of the application. 


